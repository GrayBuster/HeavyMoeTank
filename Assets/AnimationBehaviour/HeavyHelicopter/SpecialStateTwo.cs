﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialStateTwo : StateMachineBehaviour {

    private Boss boss;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss = animator.GetComponent<Boss>();
        if(boss.healthStat.CurrentValue <= boss.healthStat.MaxValue * 0.7)
        {
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                boss.numberOfMissles = 2;
                animator.SetTrigger("special");
            }
            else
            {
                boss.numberOfMissles = 4;
                animator.SetTrigger("special");
            }
        }
        else if(boss.healthStat.CurrentValue <= boss.healthStat.MaxValue * 0.3)
        {
           
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                boss.numberOfMissles = 4;
                animator.SetTrigger("special");
            }
            else
            {
                boss.numberOfMissles = 8;
                animator.SetTrigger("special");
            }
        }
        //animator.ResetTrigger("special");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
