﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour {

    private AudioSource audioSource;

    public AudioSource AudioSource { get
        {
            return audioSource;
        }
    }

    [SerializeField]
    private int point;

    public int Point
    {
        get
        {
            return point;
        }
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ground")
        {
            Destroy(gameObject);
        }
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
