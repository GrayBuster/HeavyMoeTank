﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning : MonoBehaviour {

    private float timer;

    private float indicatingWaring = 2f;

    // Update is called once per frame
    void Update () {
        timer += Time.time;
        if(timer >= indicatingWaring)
        {
            gameObject.SetActive(false);
        }
    }


    
}
