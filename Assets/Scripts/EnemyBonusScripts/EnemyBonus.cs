﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBonus : Enemy
{



    public override void UponDeath()
    {
        if (dropItems.Length > 0)
        {
            var chosenItem = Random.Range(0, dropItems.Length);
            Instantiate(dropItems[chosenItem], transform.position, Quaternion.identity);
        }
    }

    public override IEnumerator IndicatingTakingDamage()
    {
        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 0, 240);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 200);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = currentColor;
    }

    public override void Start()
    {
        base.Start();
        health = 200;
    }
    public override IEnumerator TakingDamage()
    {
        return base.TakingDamage();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
