﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScrolling : MonoBehaviour {

    public float scrollSpeed = 1;

    private Material mat;
    private Vector2 offset;

	// Use this for initialization
	void Awake () {
        mat = GetComponent<Renderer>().material;
	}
    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update () {
        offset = new Vector2(Time.time * scrollSpeed, 0);
        mat.mainTextureOffset = offset;
	}
}
