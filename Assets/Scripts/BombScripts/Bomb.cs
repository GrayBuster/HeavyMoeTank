﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

    [SerializeField]
    private float speed;

    private Animator myAnimator;

    private int hitCount = 2;

    private Rigidbody2D myBody;

    private Color currentColor;

    private void Awake()
    {
        myAnimator = GetComponent<Animator>();

        myBody = GetComponent<Rigidbody2D>();
        currentColor = GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update () {
        myBody.velocity = Vector2.down * speed;
	}

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "PlayerAmmo")
        {
            StartCoroutine(HittingBomb());
        }
        else if (target.tag == "Ground" || target.tag == "Player")
        {
            StartCoroutine(ExplosionBomb());
        }
        if (target.tag == "lazer")
        {
            StartCoroutine(ExplosionBomb());
        }
    }

    public IEnumerator HittingBomb()
    {
        hitCount -= 1;
        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 220);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 200);

        yield return new WaitForSeconds(.2f);

        transform.GetComponent<SpriteRenderer>().color = currentColor;
        if (hitCount <= 0)
        {
            StartCoroutine(ExplosionBomb());
        }
        yield return null;
    }

    public IEnumerator ExplosionBomb()
    {
        if (myAnimator != null)
        {
            myAnimator.SetTrigger("explode");
            yield return new WaitForSeconds(0.1f);
            gameObject.SetActive(false);
        }
    }
}
