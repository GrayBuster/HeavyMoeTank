﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    private static MenuController instance;

    [SerializeField]
    private GameObject settingPanel;

    [SerializeField]
    private GameObject newGamePanel;

    [SerializeField]
    private GameObject warningPanel;

    [SerializeField]
    private GameObject succesfulPanel;

    [SerializeField]
    private GameObject loadGameButton;

    [SerializeField]
    private GameObject restartButton;

    [SerializeField]
    public GameObject modePanel;

    private int highscoreGameplayOne;

    private int highscoreGameplayTwo;

    public GameObject levelOneBtn;

    public GameObject levelTwoBtn;

    public static MenuController Intsance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MenuController>();
                DontDestroyOnLoad(instance);
            }
            return instance;
        }
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(instance);
        }
    }

    private void Update()
    {
        if (PlayerPrefs.HasKey("IsGameStartForTheFirstTime") && PlayerPrefs.HasKey("IsCompletedLevel"))
        {
            if (loadGameButton != null)
            {
                loadGameButton.GetComponent<Button>().interactable = true;
                restartButton.GetComponent<Button>().interactable = true;
            }
        }
        else
        {
            loadGameButton.GetComponent<Button>().interactable = false;
            restartButton.GetComponent<Button>().interactable = false;
        }
    }

    public void NewGameScene()
    {
        IsGameStartForTheFirstTime();
        highscoreGameplayOne = 10000;
        highscoreGameplayTwo = 20000;
        if (newGamePanel != null)
        {
            newGamePanel.SetActive(true);
            if (PlayerPrefs.HasKey("IsCompletedLevel"))
            {
                if (PlayerPrefs.GetInt("IsCompletedLevel1") == 1)
                {
                    //foreach (var button in levelOneBtn.GetComponentsInChildren<Button>())
                    //{

                    //    button.interactable = true;
                    //}
                    if (PlayerPrefs.GetInt("High Score") <= highscoreGameplayOne * 0.3)
                    {
                        levelOneBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                    }
                    if (PlayerPrefs.GetInt("High Score") <= highscoreGameplayOne * 0.5)
                    {
                        levelOneBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                        levelOneBtn.GetComponentsInChildren<Button>()[2].interactable = true;
                    }
                    if (PlayerPrefs.GetInt("High Score") >= highscoreGameplayOne * 0.7)
                    {
                        levelOneBtn.GetComponentsInChildren<Button>()[3].interactable = true;
                        levelOneBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                        levelOneBtn.GetComponentsInChildren<Button>()[2].interactable = true;
                    }

                    levelTwoBtn.GetComponent<Button>().interactable = true;
                }
                if (PlayerPrefs.GetInt("IsCompletedLevel2") == 2)
                {
                    //foreach (var button in levelTwoBtn.GetComponentsInChildren<Button>())
                    //{
                    //    button.interactable = true;
                    //}
                    if (PlayerPrefs.GetInt("High Score") <= highscoreGameplayTwo * 0.3)
                    {
                        levelTwoBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                    }
                    if (PlayerPrefs.GetInt("High Score") <= highscoreGameplayTwo * 0.5)
                    {
                        levelTwoBtn.GetComponentsInChildren<Button>()[2].interactable = true;
                        levelTwoBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                    }
                    if (PlayerPrefs.GetInt("High Score") >= highscoreGameplayTwo * 0.7)
                    {
                        levelTwoBtn.GetComponentsInChildren<Button>()[3].interactable = true;
                        levelTwoBtn.GetComponentsInChildren<Button>()[1].interactable = true;
                        levelTwoBtn.GetComponentsInChildren<Button>()[2].interactable = true;
                    }
                }
            }

        }
    }

    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        //if (File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
        //{
        //    File.Delete(Application.persistentDataPath + "/PlayerTank.save");
        //}
        Time.timeScale = 1;

        //MakeInstance();
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey("IsGameStartForTheFirstTime"))
        {
            loadGameButton.GetComponent<Button>().interactable = false;
        }
    }

    public void IndicatingOption(int option)
    {
        if (option == 0)
        {
            warningPanel.SetActive(false);
            succesfulPanel.SetActive(true);
            ResetGame();
        }
        else if (option == 1)
        {
            warningPanel.SetActive(false);
        }
        else if (option == 3)
        {
            succesfulPanel.SetActive(false);
        }
        else
        {
            warningPanel.SetActive(true);
        }
    }

    void ResetGame()
    {
        PlayerPrefs.DeleteAll();
        if (File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
        {
            File.Delete(Application.persistentDataPath + "/PlayerTank.save");
        }
        succesfulPanel.SetActive(true);
    }

    public void ChoosingMode(string mode)
    {
        if (mode.Equals("Easy"))
        {
            PlayerPrefs.SetString("Mode", mode);
        }
        else if (mode.Equals("Hard"))
        {
            PlayerPrefs.SetString("Mode", mode);
        }
        LevelLoader.Instance.LoadingScene(level);
        modePanel.SetActive(false);
    }

    void IsGameStartForTheFirstTime()
    {
        if (!PlayerPrefs.HasKey("IsGameStartForTheFirstTime"))
        {
            print("first");
            PlayerPrefs.SetInt("HighScore", 0);
            PlayerPrefs.SetInt("IsGameStartForTheFirstTime", 0);
            PlayerPrefs.SetInt("IsCompletedLevel", 0);
            PlayerPrefs.SetInt("IsCompletedLevel1", 0);
            PlayerPrefs.SetInt("IsCompletedLevel2", 0);
            PlayerPrefs.SetInt("LevelCount", 1);
            PlayerPrefs.SetString("Mode", "");
        }
        //else
        //{
        //    PlayerPrefs.DeleteAll();
        //    PlayerPrefs.SetInt("HighScore", 0);
        //    loadGameButton.GetComponent<Button>().interactable = false;
        //    PlayerPrefs.SetInt("IsGameStartForTheFirstTime", 0);
        //    PlayerPrefs.SetInt("IsCompletedLevel", 0);
        //    PlayerPrefs.SetInt("LevelCount", 1);
        //}
    }

    public void ReturnHomeScene()
    {
        newGamePanel.SetActive(false);
        modePanel.SetActive(false);
    }

    int level;

    public void LoadGameMission(int level)
    {
        this.level = level;
        if (PlayerPrefs.HasKey("Mode") && PlayerPrefs.GetString("Mode").Equals("Easy") || PlayerPrefs.GetString("Mode").Equals("Hard"))
        {
            LevelLoader.Instance.LoadingScene(level);
        }
        else
        {
            newGamePanel.SetActive(false);
            modePanel.SetActive(true);
        }
    }

    public void LoadCurrentMission()
    {
        if (PlayerPrefs.HasKey("Mode") && PlayerPrefs.GetString("Mode").Equals("Easy") || PlayerPrefs.GetString("Mode").Equals("Hard"))
        {
            LevelLoader.Instance.LoadingScene(PlayerPrefs.GetInt("LevelCount"));
        }
        else
        {
            newGamePanel.SetActive(false);
            modePanel.SetActive(true);
        }
    }

    public void LoadSettingPanel()
    {
        if (settingPanel != null)
        {
            settingPanel.SetActive(true);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
