﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void EventHandler();
public class TankController : Character
{
    public Tank tank;

    [SerializeField]
    public FixedJoystick joystick;

    public float laserTimer = 0;

    public float laserDuration;

    private float direction;

    private float immortalCooldown;

    public EventHandler DeadEventHandler;

    public EventHandler GameoverEventHandler;

    private Vector3 respawnPos;

    private float nextShot;

    private float shotCoolDown = 0.125F;

    [SerializeField]
    private GameObject floatingPoint;

    [SerializeField]
    private GameObject mobileBtn;


    public void OnDead()
    {
        if (DeadEventHandler != null)
        {
            DeadEventHandler();
        }
    }

    public void IsOver()
    {
        if (GameoverEventHandler != null)
        {
            GameoverEventHandler();
        }
    }

    public static TankController Tank;

    void MakeInstance()
    {
        if (Tank == null)
        {
            Tank = FindObjectOfType<TankController>();
        }
    }

    public override bool IsDead
    {
        get
        {
            if (health <= 0)
            {
                OnDead();
            }
            return health <= 0;
        }
    }

    public bool IsGameover
    {
        get
        {
            if (tank.Life <= 0)
            {
                IsOver();
            }
            return tank.Life <= 0;
        }
    }



    private void Awake()
    {
        if (PlayerPrefs.GetString("Mode").Equals("Easy"))
        {
            if (File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/PlayerTank.save", FileMode.Open);
                SaveTank save = bf.Deserialize(file) as SaveTank;
                file.Close();
                tank = save.Tank;
                immortalCooldown = 2f;
            }
            else
            {
                immortalCooldown = 2f;
                tank = new Tank(4);
            }
        }
        else
        {
            if (File.Exists(Application.persistentDataPath + "/PlayerTank.save"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/PlayerTank.save", FileMode.Open);
                SaveTank save = bf.Deserialize(file) as SaveTank;
                file.Close();
                tank = save.Tank;
                immortalCooldown = 1.5f;
            }
            else
            {
                
                tank = new Tank(2);
                immortalCooldown = 1.5f;
            }
        }
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        StartCoroutine(RespawningImmortality());
        MakeInstance();
        respawnPos = transform.position;
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        if (!IsDead)
        {
            HandleMovement(horizontal);
        }

    }

    public void IndicatingGameover()
    {
        Cursor.visible = true;
        GameplayController1.Gameplay1.gameoverPanel.SetActive(true);
        GameplayController1.Gameplay1.pauseBtn.SetActive(false);
        GameplayController1.Gameplay1.gameoverPanel.transform.GetChild(3).GetComponent<Text>().text = GameplayController1.Gameplay1.scoreTxt.text;
        if (TankController.Tank.tank.Point > GameplayController1.Gameplay1.GetHighScore())
        {
            GameplayController1.Gameplay1.SetHighScore(TankController.Tank.tank.Point);
            GameplayController1.Gameplay1.gameoverPanel.transform.GetChild(5).GetComponent<Text>().text = GameplayController1.Gameplay1.GetHighScore().ToString();
        }
        else
        {
            GameplayController1.Gameplay1.gameoverPanel.transform.GetChild(5).GetComponent<Text>().text = GameplayController1.Gameplay1.scoreTxt.text;
        }
        PlayerPrefs.SetInt("IsCompletedLevel", GameplayController1.Gameplay1.LevelCount);
        if (TankController.Tank.tank.Point > GameplayController1.Gameplay1.GetHighScore())
        {
            GameplayController1.Gameplay1.SetHighScore(TankController.Tank.tank.Point);
        }
        else
        {
            GameplayController1.Gameplay1.SetHighScore(TankController.Tank.tank.Point);
        }
    }

    private void OnGUI()
    {
        if (IsGameover)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
        }
    }

    private void Update()
    {
        if (!IsDead)
        {
            nextShot += Time.deltaTime;
            HandleInput();
            if (tank.LargeBullet)
            {
                transform.localScale = new Vector3(2.3f, 2.3f, 1);
            }
            if (tank.Laser)
            {
                laserTimer += Time.deltaTime;
                if (laserTimer >= laserDuration)
                {
                    laserTimer = 0;
                    tank.Laser = false;
                }
            }
            
            if (tank.Shield)
            {
                transform.Find("Shield").gameObject.SetActive(true);
                
            }
           
        }

    }

    private IEnumerator IndicatingImmortality()
    {
        while (tank.Immortal)
        {
            transform.GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(.1f);

            transform.GetComponent<SpriteRenderer>().enabled = true;

            yield return new WaitForSeconds(.1f);
        }
    }

    void HandleMovement(float horizontal)
    {
#if UNITY_STANDALONE
        if (!IsDead)
        {
            //float Xtemp = transform.position.x;
            //Vector3 touchPostion = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //float delta = touchPostion.x - Xtemp;
            //MyRidgidbody.velocity = new Vector2(Time.deltaTime * delta * movementSpeed, MyRidgidbody.velocity.y);
            MyRidgidbody.velocity = new Vector2(horizontal * movementSpeed * Time.deltaTime, MyRidgidbody.velocity.y);
            MyAnimator.SetFloat("move", Mathf.Abs(horizontal));
        }
        else
        {
            MyRidgidbody.velocity = Vector2.zero;
        }
#endif
#if UNITY_ANDROID
        if (!IsDead)
        {
            MyRidgidbody = GetComponent<Rigidbody2D>();
            MyRidgidbody.velocity = new Vector2(joystick.Horizontal * movementSpeed * Time.deltaTime, MyRidgidbody.velocity.y);
        }
        else
        {
            MyRidgidbody.velocity = Vector2.zero;
        }
#endif
    }

    public void RemovingBtn()
    {
        movingLeft = false;
        movingRight = false;
    }

    public bool movingRight;

    public bool movingLeft;

    public void MovingLeft()
    {
        movingLeft = true; 
    }

    public void MovingRight()
    {
        movingRight = true;
    }


    void HandleInput()
    {

#if UNITY_STANDALONE
        mobileBtn.SetActive(false);
        if (Input.GetMouseButton(0))
        {
            if (!IsDead && Time.timeScale > 0)
            {
                if (!tank.Laser)
                {
                    if (nextShot >= shotCoolDown)
                    {
                        GunController.Gun.ShootingBullet();
                        nextShot = 0;
                    }
                }

            }
        }

#endif
#if UNITY_ANDROID
        mobileBtn.SetActive(true);
        if (!IsDead && Time.timeScale > 0)
        {
            if (GameplayController1.Gameplay1.guidePanel != null)
            {
                if (!GameplayController1.Gameplay1.guidePanel.activeInHierarchy)
                {
                    if (!tank.Laser && GunController.Gun.joybutton.pressed)
                    {
                        if (nextShot >= shotCoolDown)
                        {
                            GunController.Gun.ShootingBullet();
                            nextShot = 0;
                        }
                    }
                }
            }
            else
            {
                if (!tank.Laser)
                {
                    if (nextShot >= shotCoolDown && GunController.Gun.joybutton.pressed)
                    {
                        GunController.Gun.ShootingBullet();
                        nextShot = 0;
                    }
                }
            }
        }
#endif
    }



    public override void Respawn()
    {
        MyRidgidbody.velocity = Vector2.zero;
        MyAnimator.SetTrigger("idle");
        health = 30;
        transform.position = respawnPos;
        StartCoroutine(RespawningImmortality());
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Enemy" || target.gameObject.tag == "EnemyAmmo")
        {
            Physics2D.IgnoreCollision(target.gameObject.GetComponent<Collider2D>(), transform.GetComponent<Collider2D>(), true);
        }
    }

    public override void OnTriggerEnter2D(Collider2D target)
    {
        if (damageSources.Contains(target.tag))
        {
            if (!IsDead)
            {
                StartCoroutine(TakingDamage());
            }
        }
        if (target.gameObject.tag == "Item")
        {
            if (!IsDead)
            {
                if (!tank.Laser)
                {
                    if (target.name.Contains("HeavyBullet") && tank.LargeBullet)
                    {
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        tank.Point += item.Point;
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                    }
                    else if (target.name.Contains("HeavyBullet"))
                    {
                        tank.LargeBullet = true;
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        AudioSource.PlayClipAtPoint(item.AudioSource.clip, transform.position);
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        string[] splitString = target.name.Split('(');
                        clone.GetComponent<FloatingPoint>().thingToFloat = splitString[0];
                        tank.DealingDamage *= 2;
                    }
                    if (target.name.Contains("ExtraShot") && tank.ExtraShot)
                    {
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        tank.Point += item.Point;
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                    }
                    else if (target.name.Contains("ExtraShot"))
                    {
                        tank.ExtraShot = true;
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        AudioSource.PlayClipAtPoint(item.AudioSource.clip, transform.position);
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        string[] splitString = target.name.Split('(');
                        clone.GetComponent<FloatingPoint>().thingToFloat = splitString[0];
                    }
                    if (target.name.Contains("RocketLauncher") && tank.RocketLauncher)
                    {
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        tank.Point += item.Point;
                        clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                    }
                    else if (target.name.Contains("RocketLauncher"))
                    {
                        tank.RocketLauncher = true;
                        var item = target.gameObject.GetComponent<Items>() as Items;
                        AudioSource.PlayClipAtPoint(item.AudioSource.clip, transform.position);
                        var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                        clone.transform.position = transform.position;
                        string[] splitString = target.name.Split('(');
                        clone.GetComponent<FloatingPoint>().thingToFloat = splitString[0];
                    }
                    else if (target.name.Contains("Life"))
                    {
                        if (tank.Life < 4)
                        {
                            tank.Life++;
                            var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                            clone.transform.position = transform.position;
                            string[] splitString = target.name.Split('(');
                            clone.GetComponent<FloatingPoint>().thingToFloat = "1 " + splitString[0];
                        }
                        else
                        {
                            var item = target.gameObject.GetComponent<Items>() as Items;
                            var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                            clone.transform.position = transform.position;
                            tank.Point += item.Point;
                            clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                        }
                    }
                }
                else
                {
                    var item = target.gameObject.GetComponent<Items>() as Items;
                    var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                    clone.transform.position = transform.position;
                    clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                }

                if (target.name.Contains("Shield") && tank.Shield)
                {
                    var item = target.gameObject.GetComponent<Items>() as Items;
                    var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                    clone.transform.position = transform.position;
                    tank.Point += item.Point;
                    clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                }
                else if (target.name.Contains("Shield"))
                {
                    tank.Shield = true;
                    tank.ShieldDuration = 2;
                    transform.Find("Shield").gameObject.SetActive(true);
                    var item = target.gameObject.GetComponent<Items>() as Items;
                    AudioSource.PlayClipAtPoint(item.AudioSource.clip, transform.position);
                    var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                    clone.transform.position = transform.position;
                    string[] splitString = target.name.Split('(');
                    clone.GetComponent<FloatingPoint>().thingToFloat = splitString[0];
                }
                if (target.name.Contains("Lazer") && tank.Laser)
                {
                    var item = target.gameObject.GetComponent<Items>() as Items;
                    var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                    clone.transform.position = transform.position;
                    tank.Point += item.Point;
                    clone.GetComponent<FloatingPoint>().thingToFloat = item.Point.ToString();
                }
                else if (target.name.Contains("Lazer"))
                {
                    tank.Laser = true;
                    var item = target.gameObject.GetComponent<Items>() as Items;
                    laserDuration = 10f;
                    AudioSource.PlayClipAtPoint(item.AudioSource.clip, transform.position);
                    var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                    clone.transform.position = transform.position;
                    string[] splitString = target.name.Split('(');
                    clone.GetComponent<FloatingPoint>().thingToFloat = splitString[0];
                }
            }
            else
            {
                var clone = Instantiate(floatingPoint, transform.position, Quaternion.identity);
                clone.transform.position = transform.position;
                clone.GetComponent<FloatingPoint>().thingToFloat = "You're dead.\nYou can't pick an item.";
            }
        }

    }


    private IEnumerator RespawningImmortality()
    {
        tank.Immortal = true;
        StartCoroutine(IndicatingImmortality());
        yield return new WaitForSeconds(2);

        tank.Immortal = false;
    }

    private IEnumerator IndicatingShield()
    {
        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;

        yield return new WaitForSeconds(.2f);

        transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;

        yield return new WaitForSeconds(.2f);
       
    }


    public override IEnumerator TakingDamage()
    {
        if (tank.Shield)
        {
            if(tank.ShieldDuration > 0)
            {
                tank.ShieldDuration--;
                StartCoroutine(IndicatingShield());
            }
            else if (tank.ShieldDuration == 0)
            {
                transform.GetChild(1).gameObject.SetActive(false);
                tank.Shield = false;
            }
        }
        else
        {
            if (!tank.Immortal)
            {
                health -= 10;
                if (!IsDead)
                {
                    StartCoroutine(IndicatingTakingDamage());
                    tank.Immortal = true;
                    StartCoroutine(IndicatingImmortality());
                    yield return new WaitForSeconds(immortalCooldown);

                    tank.Immortal = false;

                }
                else
                {
                    StartCoroutine(IndicatingDeath());
                    tank.Life -= 1;
                }
            }
        }
        yield return null;
    }




    public override IEnumerator IndicatingTakingDamage()
    {
        MyAnimator.SetTrigger("damage");
        yield return null;
    }

    public override IEnumerator IndicatingDeath()
    {
        MyAnimator.SetTrigger("dead");
        yield return null;
    }
}
