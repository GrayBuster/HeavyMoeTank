﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private Vector3 offset;

    private Transform target;

    // Use this for initialization
    void Start () {
        target = GameObject.Find("Tank").transform;
        offset = transform.position - target.position;
    }

    private void Update()
    {
        if(GameObject.Find("Tank") != null)
        {
            target = GameObject.Find("Tank").transform;
        }
    }

    // Update is called once per frame
    void LateUpdate () {
        //transform.position = new Vector3(Mathf.Clamp(target.position.x, xMin, xMax), Mathf.Clamp(target.position.y, yMin, yMax),transform.position.z);
        transform.position = target.transform.position + offset;
    }
}
