﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Tank  {

    //Mark : Properties

    public int Point { get; set; }

    public int ShieldDuration { get; set; }

    public bool LargeBullet { get; set; }

    public bool ExtraShot { get; set; }

    public bool RocketLauncher { get; set; }

    public bool Laser { get; set; }

    public int Life { get; set; }

    public int DealingDamage { get; set; }

    public int RocketDamage { get; set; }

    public bool Immortal { get; set; }

    public bool Shield { get; set; }

    public Tank(Tank tank)
    {
        Life = tank.Life;
        DealingDamage = tank.DealingDamage;
        RocketDamage = tank.RocketDamage;
        ShieldDuration = 2;
        Point = tank.Point;

        LargeBullet = tank.LargeBullet;

        ExtraShot = tank.ExtraShot;

        RocketLauncher = tank.RocketLauncher;

        Laser = tank.Laser;

        Immortal = tank.Immortal;

        Shield = tank.Shield;
    }

    public Tank(int life)
    {
        Life = life;
        DealingDamage = 30;
        RocketDamage = 0;
        ShieldDuration = 2;
        Point = 0;

        LargeBullet = false;

        ExtraShot = false;

        RocketLauncher = false;

        Laser = false;

        Immortal = true;

        Shield = false;
    }

}
