﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour {

    [SerializeField]
    protected List<string> damageSources;

    [SerializeField]
    protected int health;

    [SerializeField]
    protected float movementSpeed;

    [SerializeField]
    public bool facingRight;

    public Animator MyAnimator { get; private set; }

    public Rigidbody2D MyRidgidbody { get; set; }

    public abstract bool IsDead { get; }

    // Use this for initialization
    public virtual void Start () {
        MyAnimator = GetComponent<Animator>();
        MyRidgidbody = GetComponent<Rigidbody2D>();
        if (PlayerPrefs.HasKey("Mode"))
        {
            if (PlayerPrefs.GetString("Mode").Equals("Easy"))
            {
                if (this is TankController)
                {
                    health = 30;
                }
                else if (this is Boss)
                {
                    health = Random.Range(10000, 15000);
                }
                else if (this is BraveGurerrier)
                {
                    health = Random.Range(20000, 25000);
                }
                else
                {
                    health = Random.Range(100, 150);
                }
            }
            else
            {
                if (this is TankController)
                {
                    health = 30;
                }
                else if (this is Boss)
                {
                    health = Random.Range(65000, 75000);
                }
                else if (this is BraveGurerrier)
                {
                    health = Random.Range(100000, 150000);
                }
                else
                {
                    health = Random.Range(400, 250);
                }
            }
        }
       
    }

    public abstract IEnumerator TakingDamage();

    public abstract IEnumerator IndicatingTakingDamage();

    public abstract IEnumerator IndicatingDeath();
   
    public virtual void ChangeDirection()
    {
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, 1);
    }

    public abstract void OnTriggerEnter2D(Collider2D collision);


    public virtual void Respawn()
    {
        Destroy(gameObject);
    }
    
}
