﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

    private static LevelLoader levelLoader;

    public static LevelLoader Instance
    {
        get
        {
            if (levelLoader == null)
            {
                levelLoader = FindObjectOfType<LevelLoader>();
            }
            return levelLoader;
        }
    }

    public GameObject loadingScene;

    public Slider loadingSlider;

    public Text progressTxt;

    public GameObject background;

    public GameObject tankAnimation;

    public void LoadingSceneWithString(string scene,GameObject pauseCanvas)
    {
        Time.timeScale = 1;
        if (pauseCanvas.activeSelf)
        {
            pauseCanvas.SetActive(false);
        }
        GameplayController1.Gameplay1.playerCanvas.SetActive(false);
        GameplayController1.Gameplay1.completedLevelPanel.SetActive(false);
        GameplayController1.Gameplay1.gameoverPanel.SetActive(false);
        TankController.Tank.joystick.gameObject.SetActive(false);
        GunController.Gun.joybutton.gameObject.SetActive(false);
       
        StartCoroutine(LoadAsynchronously(scene));
    }

    public void LoadingScene(int levelCount)
    {
        Time.timeScale = 1;
        
        if (GameplayController1.Gameplay1 != null)
        {
            GameplayController1.Gameplay1.playerCanvas.SetActive(false);
            GameplayController1.Gameplay1.completedLevelPanel.SetActive(false);
            GameplayController1.Gameplay1.gameoverPanel.SetActive(false);
            TankController.Tank.joystick.gameObject.SetActive(false);
            GunController.Gun.joybutton.gameObject.SetActive(false);
        }
        
        StartCoroutine(LoadAsynchronously(levelCount));
    }

    IEnumerator LoadAsynchronously(string scene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        progressTxt.text = (int)(operation.progress) * 100f + "%";

        loadingScene.SetActive(true);

        background.SetActive(true);

        tankAnimation.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            progressTxt.text = (int)(progress * 100f) + "%";

            loadingSlider.value = progress;

            yield return null;
        }

        yield return new WaitForSeconds(2f);
    }

    IEnumerator LoadAsynchronously(int levelCount)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("GamePlayStage" + levelCount);

        progressTxt.text = operation.progress * 100f + "%";

        loadingScene.SetActive(true);

        background.SetActive(true);

        tankAnimation.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            progressTxt.text = (int)(progress * 100f) + "%";

            loadingSlider.value = progress;

            yield return null;
        }

        yield return new WaitForSeconds(2f);
    }
}
