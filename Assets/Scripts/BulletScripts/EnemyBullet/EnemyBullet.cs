﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyBullet : MonoBehaviour {
    [SerializeField]
    private float speed;

    private Rigidbody2D myRigidbody;

    private Animator myAnimator;

    private Vector3 direction;

    // Use this for initialization
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        if (transform.GetComponent<Animator>() != null)
        {
            myAnimator = GetComponent<Animator>();
        }
    }

    void FixedUpdate()
    {
        myRigidbody.velocity = new Vector2(direction.x, direction.y).normalized * speed;
    }

    public void Initialize(Vector3 direction)
    {
        this.direction = direction;
    }

    public IEnumerator ExplosionAmmo()
    {
        if (myAnimator != null)
        {
            myAnimator.SetTrigger("explode");
            yield return new WaitForSeconds(0.05f);
            myAnimator.ResetTrigger("explode");
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Ground" || target.tag == "Player")
        {
            StartCoroutine(ExplosionAmmo());
        }
        
    }
}
