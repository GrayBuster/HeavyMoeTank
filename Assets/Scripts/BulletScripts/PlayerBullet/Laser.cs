﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    Vector2 mousePos;

    private LineRenderer line;

    private int laserDamage = 25;

    [SerializeField]
    private Material[] materialArr;

    [SerializeField]
    private AudioClip lazerSound;

    public Transform laserHit;

    // Use this for initialization
    void Start()
    {
        line = GetComponent<LineRenderer>();

        line.enabled = false;

        line.useWorldSpace = true;
    }

    // Update is called once per frame
    void Update()
    {

#if UNITY_STANDALONE
         mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition - (transform.position + Camera.main.transform.position));
        RaycastHit2D hit = Physics2D.Raycast(transform.position, mousePos);
        if (TankController.Tank.tank.Laser)
        {
            if (Input.GetMouseButton(0))
            {
                line.enabled = true;
                line.tag = "lazer";
                line.SetPosition(0, transform.position);
                StartCoroutine(IndicatingLaser());
                line.SetPosition(1, hit.point + hit.normal);
                GunController.Gun.GetComponent<AudioSource>().PlayOneShot(lazerSound);
                GunController.Gun.MyAnimator.SetTrigger("lazer");

                if (hit)
                {
                    if (hit.collider.gameObject.tag.Contains("Enemy"))
                    {
                        if (hit.collider.gameObject.GetComponent<Enemy>() != null)
                        {
                            if (!hit.collider.gameObject.GetComponent<Enemy>().IsDead)
                            {
                                StartCoroutine(hit.collider.gameObject.GetComponent<Enemy>().TakingDamageWithLaser(laserDamage));
                            }
                        }
                    }
                    if (hit.collider.gameObject.tag.Contains("Boss"))
                    {
                        if (hit.collider.gameObject.GetComponent<Enemy>() != null)
                        {
                            if (!hit.collider.gameObject.GetComponent<Enemy>().IsDead)
                            {
                                StartCoroutine(hit.collider.gameObject.GetComponent<Enemy>().TakingDamageWithLaser(laserDamage));
                            }
                        }
                    }
                    if (hit.collider.gameObject.tag.Contains("EnemyAmmo"))
                    {
                        if (hit.collider.gameObject.GetComponent<EnemyBullet>() != null)
                        {
                            StartCoroutine(hit.collider.gameObject.GetComponent<EnemyBullet>().ExplosionAmmo());
                        }
                    }
                }

            }
            else
            {
                line.enabled = false;
            }
        }
        else
        {
            line.enabled = false;
        }
#endif
#if UNITY_ANDROID
        Vector3 mousePos = new Vector3(GunController.Gun.joybutton.Horizontal, GunController.Gun.joybutton.Vertical,0);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, mousePos);
        if (TankController.Tank.tank.Laser && !TankController.Tank.IsDead && GunController.Gun.joybutton.pressed)
        {
            line.enabled = true;
            line.tag = "lazer";
            line.SetPosition(0, transform.position);
            StartCoroutine(IndicatingLaser());
            line.SetPosition(1, hit.point + hit.normal);
            GunController.Gun.audioSource.PlayOneShot(lazerSound);
            GunController.Gun.MyAnimator.SetTrigger("lazer");

            if (hit)
            {
                if (hit.collider.gameObject.tag.Contains("Enemy"))
                {
                    if (hit.collider.gameObject.GetComponent<Enemy>() != null)
                    {
                        if (!hit.collider.gameObject.GetComponent<Enemy>().IsDead)
                        {
                            StartCoroutine(hit.collider.gameObject.GetComponent<Enemy>().TakingDamageWithLaser(laserDamage));
                        }
                    }
                }
                if (hit.collider.gameObject.tag.Contains("Boss"))
                {
                    if (hit.collider.gameObject.GetComponent<Enemy>() != null)
                    {
                        if (!hit.collider.gameObject.GetComponent<Enemy>().IsDead)
                        {
                            StartCoroutine(hit.collider.gameObject.GetComponent<Enemy>().TakingDamageWithLaser(laserDamage));
                        }
                    }
                }
                if (hit.collider.gameObject.tag.Contains("EnemyAmmo"))
                {
                    if (hit.collider.gameObject.GetComponent<EnemyBullet>() != null)
                    {
                        StartCoroutine(hit.collider.gameObject.GetComponent<EnemyBullet>().ExplosionAmmo());
                    }
                }
            }

            else
            {
                line.enabled = false;
            }
        }
        else
        {
            line.enabled = false;
        }
#endif

    }

    IEnumerator IndicatingLaser()
    {
        for (int i = 0; i < materialArr.Length; i++)
        {
            line.material = materialArr[i];
            yield return new WaitForSeconds(.5f);
        }
    }

}
