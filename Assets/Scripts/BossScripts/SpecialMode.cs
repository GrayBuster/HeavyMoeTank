﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialMode : IEnemyState {

    private Boss boss;

    private float specialModeTimer;

    private float specialModeDuration = 2f;

    private void Special()
    {
        specialModeTimer += Time.deltaTime;
        if (specialModeTimer >= specialModeDuration)
        {
            boss.ChangeState(new BossIdleState());
        }
    }

    public void Enter(Enemy enemy)
    {
        boss = (Boss)enemy;
        float xDir = boss.Target.transform.position.x - boss.transform.position.x;
        if (xDir < 0 && boss.facingRight || xDir > 0 && !boss.facingRight)
        {
            boss.ChangeDirection();
        }
        boss.MyAnimator.SetTrigger("special");
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        Special();
    }


    public void Exit()
    {
    }

}
