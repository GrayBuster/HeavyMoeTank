﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBombMode : IEnemyState
{
    private float timer;

    private float timeDuration = 1f;

    private Boss boss;

    void IEnemyState.Enter(Enemy enemy)
    {
        this.boss = (Boss)enemy;
        timer = 0;
        boss.StartCoroutine(boss.InitializeBomb());
    }

    void IEnemyState.EnterTriggerEnter2D(Collider2D other)
    {

    }

    void DropsBomb()
    {
        if (timer >= timeDuration)
        {
            boss.ChangeState(new BossIdleState());
        }
    }

    void IEnemyState.Execute()
    {
        timer += Time.deltaTime;
        boss.Move();
        DropsBomb();
    }

    void IEnemyState.Exit()
    {

    }
}
