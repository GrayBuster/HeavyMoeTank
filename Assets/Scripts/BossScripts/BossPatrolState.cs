﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPatrolState : IEnemyState {

    private Boss boss;

    private float patrolTimer;

    private float patrolDuration = 2f;

    private void Patroling()
    {
        boss.MyAnimator.SetFloat("flying", 2);
        boss.Move();
        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolDuration)
        {
            if (Random.Range(0, 1) == 0)
            {
                boss.ChangeState(new DropBombMode());
            }
            else
            {
                boss.ChangeState(new BossIdleState());
            }
        }
    }

    public void Enter(Enemy enemy)
    {
        boss = (Boss)enemy;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {

    }

    public void Execute()
    {
        Patroling();
    }

    public void Exit()
    {
    }

}
