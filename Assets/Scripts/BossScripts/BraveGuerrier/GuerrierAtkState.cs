﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuerrierAtkState : IEnemyState {

    private BraveGurerrier gurerrier;

    private float timer;

    public void Enter(Enemy enemy)
    {
        this.gurerrier = (BraveGurerrier)enemy;
        timer = 0;
        if(gurerrier.Target != null)
        {
            gurerrier.MyAnimator.SetTrigger("attack");
        }
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        timer += Time.deltaTime;
    }

    public void Exit()
    {
    }
    
}
