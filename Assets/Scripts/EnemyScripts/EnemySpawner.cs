﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField]
    private bool rightSpawner;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(EnemySpawning());
    }

    void SpawnSupplyHelicopter()
    {
        if (Random.Range(0, 100) < 15)
        {
            var enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "HelicopterSupplier");
            enemy.transform.position = transform.position;
            enemy.SetActive(true);
        }
    }

    IEnumerator EnemySpawning()
    {
        if (rightSpawner)
        {
            yield return new WaitForSeconds(1f);

            GameObject enemy;
            var randomIndex = Random.Range(0, 3);
            if (randomIndex == 0)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "HelicopterFacingLeft");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }
            else if (randomIndex == 1)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "UFOFacingLeft");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }
            else if (randomIndex == 2)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "B52FacingLeft");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }
        }
        else
        {
            GameObject enemy;
            var randomIndex = Random.Range(0, 3);
            if (randomIndex == 0)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "Helicopter");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }
            else if (randomIndex == 1)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "UFO");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }
            else if (randomIndex == 2)
            {
                enemy = ObjectPooler.SharedInstance.GetPooledObject("Enemy", "B52");
                enemy.transform.position = transform.position;
                enemy.SetActive(true);
            }

            SpawnSupplyHelicopter();
            yield return new WaitForSeconds(1f);


        }
        yield return new WaitForSeconds(2f);
        StartCoroutine(EnemySpawning());
    }
}
