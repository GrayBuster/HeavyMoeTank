﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTargetState : IEnemyState {

    private Enemy enemy;

    private float followingTargetTimer;

    private float followingTargetDuration = 2;

    public void Enter(Enemy enemys)
    {
        this.enemy = enemys;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
    }

    public void Execute()
    {
        if(enemy.Target != null)
        {
            Targeting();
        }
    }

    private void Targeting()
    {
        followingTargetTimer += Time.deltaTime;
        enemy.MyAnimator.SetTrigger("targeting");
        if (followingTargetTimer >= followingTargetDuration)
        {
            followingTargetTimer = 0;
        }
        enemy.ChangeState(new AttackState());
    }

    public void Exit()
    {
    }

    
}
