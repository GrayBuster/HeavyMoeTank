﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState
{
    private float patrolTimer;

    private float patrolDuration = 1.5f;

    private Enemy enemy;

    public void Enter(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {
        return;
    }

    public void Execute()
    {
        Patroling();
        enemy.Move();
    }

    public void Exit()
    {
    }

    private void Patroling()
    {
        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolDuration)
        {
            enemy.ChangeState(new AttackState());
        }
    }
}
