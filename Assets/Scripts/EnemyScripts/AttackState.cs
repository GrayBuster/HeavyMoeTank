﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState
{
    private Enemy enemy;

    public void Enter(Enemy enemys)
    {
        this.enemy = enemys;
        enemy.MyAnimator.SetTrigger("attack");
    }

    public void EnterTriggerEnter2D(Collider2D other)
    {

    }

    public void Execute()
    {
        if (enemy.Target != null)
        {
            Shooting();
        }

    }

    private void Shooting()
    {
        enemy.ChangeState(new PatrolState());
    }

    public void Exit()
    {

    }
}
